package br.com.ericocm.commons.repository;

import br.com.ericocm.commons.model.Customer;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CustomerPagingRepository extends PagingAndSortingRepository<Customer, Integer> {


}
