package br.com.ericocm.commons.config.validation;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class RequestErrorDto {

    private String fieldName;
    private String errorMessage;

}
