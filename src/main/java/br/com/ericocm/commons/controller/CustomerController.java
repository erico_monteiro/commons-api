package br.com.ericocm.commons.controller;

import br.com.ericocm.commons.model.Customer;
import br.com.ericocm.commons.repository.CustomerPagingRepository;
import br.com.ericocm.commons.repository.CustomerRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@Api(value = "Customers")
@RestController
@RequestMapping("/customers")
public class CustomerController {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CustomerPagingRepository customerPagingRepository;

    @ApiOperation(value = "List customers (all, by name or by document)")
    @GetMapping
    public List<Customer> list(String document, String name) {

        if (name == null && document == null) {
            return customerRepository.findAll();
        }

        if (name == null) {
            return customerRepository.findByDocument(document);
        }

        if (document == null) {
            return customerRepository.findByNameContaining(name);
        }

        return customerRepository.findByNameContainingAndDocument(name, document);

    }

    @ApiOperation(value = "List customers (all, paging)")
    @GetMapping("/paging")
    public Page<Customer> listPaging(Integer page, Integer size) {

        return customerPagingRepository.findAll(PageRequest.of(page, size, Sort.by("name")));

    }

    @ApiOperation(value = "Get a customer by ID")
    @GetMapping("/{id}")
    public ResponseEntity<Customer> getById(@PathVariable Long id) {
        Optional<Customer> customer = customerRepository.findById(id);

        if (customer.isPresent()) {
            return ResponseEntity.ok(customer.get());
        }

        return ResponseEntity.notFound().build();
    }

    @ApiOperation(value = "Insert a new customer")
    @PostMapping
    @Transactional
    public ResponseEntity<Customer> insert(@RequestBody @Valid Customer customer, UriComponentsBuilder uriComponentsBuilder) {
        customerRepository.save(customer);
        URI uri = uriComponentsBuilder.path("/customers/{id}").buildAndExpand(customer.getId()).toUri();
        return ResponseEntity.created(uri).body(customer);
    }

    @ApiOperation(value = "Update a customer by ID")
    @PutMapping("/{id}")
    @Transactional
    public ResponseEntity<Customer> update(@PathVariable Long id, @RequestBody @Valid Customer customer) {

        if (customerRepository.existsById(id)) {
            customer.setId(id);
            customerRepository.save(customer);
            return ResponseEntity.ok(customer);
        }

        return ResponseEntity.notFound().build();
    }

    @ApiOperation(value = "Delete a customer by ID")
    @DeleteMapping("/{id}")
    @Transactional
    public ResponseEntity<?> delete(@PathVariable Long id) {

        if (customerRepository.existsById(id)) {
            customerRepository.deleteById(id);
            return ResponseEntity.ok().build();
        }

        return ResponseEntity.notFound().build();
    }

}
