# Commons API #
This project its a micro-service to expose the commons features:
- Customers

### Requires ###
* Java 14
* Docker - https://docs.docker.com/install/
* Docker compose - https://docs.docker.com/compose/install/
* Maven - https://maven.apache.org/download.cgi

## Execute docker-compose for local environment
The docker-compose file its located under src/main/docker
```sh
docker-compose up -d
```
This command its gonna start:
- Postrgres server.
- Pgadmin
- Commons API (running on pont 18080)

### Start application server from code by maven ###
```sh
mvn spring-boot:run
```

After the application started access the url:
```sh
http://localhost:8080/swagger-ui.html
```
You should see the swegger documentation

#### The postman collections test file its located under src/main/postman ####

### To build a docker image ###
```sh
docker build -t ericoh/commons-api:0.0.1 .
```

### To push a docker image ###
```sh
docker push ericoh/commons-api:0.0.1
```

### Docker image ###

https://hub.docker.com/r/ericoh/commons-api
